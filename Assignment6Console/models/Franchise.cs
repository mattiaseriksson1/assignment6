﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment6Console.models
{
    public class Franchise
    {
       
        public int Id { get; set; }
        public string FranchiseName { get; set; }
        public string Description { get; set; }
        public ICollection<Movie> Movies { get; set; }

    }
}
