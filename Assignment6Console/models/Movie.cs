﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment6Console.models
{
    public class Movie
    {
     
        public int Id { get; set; }
        public string Title { get; set; }
       // [MaxLength: 400]
        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        public string DirectorName { get; set; }
        public string PictureUrl { get; set; }
        public string TrailerUrl { get; set; }
        public ICollection<Character> Characters { get; set; }
        public ICollection<Franchise> Franchises { get; set; }

    }
}
