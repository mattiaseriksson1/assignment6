﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment6Console.models
{
    public class MovieDbContext: DbContext
    {
        public DbSet<Character> Character { get; set; }
        public DbSet<Movie> Movie { get; set; }
        public DbSet<Franchise> Franchise { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

            builder.DataSource = "N-SE-01-1178";
            builder.InitialCatalog = "Movies";
            builder.IntegratedSecurity = true;
            builder.Encrypt = false;

            optionsBuilder.UseSqlServer(builder.ConnectionString);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);



            //modelBuilder.Entity<Character>(entity =>
            //{
            //    entity.HasKey(e => e.Id);
            //    entity.Property(e => e.image);
            //    entity.Property(e => e.Alias);
            //    entity.Property(e => e.Name).IsRequired();
            //    entity.Property(e => e.Gender);
            //    entity.HasMany(d => d.Movies)
            //        .WithMany(d => d.Characters);

            //});
            //modelBuilder.Entity<Franchise>(entity =>
            //{
            //    entity.HasKey(e => e.Id);
            //    entity.Property(e => e.Description);
            //    entity.Property(e => e.FranchiseName).IsRequired();
            //    entity.HasMany(d => d.Movies);

            //});
            //});
            
            modelBuilder.Entity<Movie>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.HasKey(e => e.ReleaseYear);
                entity.HasKey(e => e.TrailerUrl);
                entity.HasKey(e => e.DirectorName);
                entity.HasKey(e => e.Genre);
                entity.HasKey(e => e.PictureUrl);
                entity.Property(e => e.Title).IsRequired();
                //   entity.HasOne(e => e.Franchises);

            });


            //modelBuilder.Entity<Movie>().HasData(
            //    new Movie
            //    {
            //       Id = 2,
            //        Title = "Return of the king",
            //        DirectorName = "Peter Jackson",
            //        Genre = "Adventure",
            //        ReleaseYear = 2003,
            //        TrailerUrl = "https://www.youtube.com/watch?v=r5X-hFf6Bwo",
            //        PictureUrl = "https://static.posters.cz/image/1300/poster/the-lord-of-the-rings-kungens-aterkomst-i104633.jpg",
            //    });
            

        }

      
    }
}       
   
