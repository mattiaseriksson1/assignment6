﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Text;
using Assignment6Console.models;

namespace mysqlefcore
{
    class Program
    {
        static void Main(string[] args)
        {
           InsertData();
           PrintData();
        }

      
        
        private static void InsertData()
        {
            using (var context = new MovieDbContext())
            {
          
               context.Database.EnsureCreated();
               
        
                var Moviefranchise = new Franchise
                {
                    FranchiseName = "Lord of the rings",
                    Description = "Epic Adventure",
                    
                    
                };
                context.Franchise.Add(Moviefranchise);
                
                
                context.Movie.Add(new Movie
                {
                    
                    Title = "The Lord of the Rings",
                    DirectorName = "Peter Jackson",
                    Genre = "Adventure",
                    ReleaseYear = 2001,
                    TrailerUrl = "https://www.youtube.com/watch?v=V75dMMIW2B4",
                    PictureUrl = "https://static.posters.cz/image/1300/poster/lord-of-the-rings-fellowship-i11723.jpg",
                  // Franchises = (ICollection<Franchise>)Moviefranchise
                });
           

               
                context.SaveChanges();
            }
        }

        private static void PrintData()
        {
            
            using (var context = new MovieDbContext())
            {
                var movies = context.Movie
                  .Include(p => p.Franchises);
                foreach (var movieF in movies)
                {
                    var data = new StringBuilder();
                    data.AppendLine($"Movie ID: {movieF.Id}");
                    data.AppendLine($"Title: {movieF.Title}");
                    data.AppendLine($"Franchise: {movieF.Franchises}");
                    Console.WriteLine(data.ToString());
                }
            }
        }
    }
}